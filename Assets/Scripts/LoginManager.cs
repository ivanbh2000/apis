using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class LoginManager : MonoBehaviour
{
    public InputField emailInputField;
    public InputField passwordInputField;

    private const string loginUrl = "https://api.juaaree.com/api/v1/login";

    // Method to initiate the login process
    public void PerformLogin()
    {
        string email = emailInputField.text;
        string password = passwordInputField.text;
        StartCoroutine(LoginRoutine(email, password));
    }

    // Coroutine to handle the login API call
    private IEnumerator LoginRoutine(string email, string password)
    {
        //Creating a LoginData object to hold the request data
        LoginData loginData = new LoginData
        {
            email = email,
            password = password,
            membertype = 1,
            source = "wallet"
        };

        //Converting loginData object to JSON format
        string jsonData = JsonUtility.ToJson(loginData);

        //Creating a UnityWebRequest to send the POST request
        UnityWebRequest request = new UnityWebRequest(loginUrl, "POST");
        byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(jsonData);
        request.uploadHandler = new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");

        //Sending request
        yield return request.SendWebRequest();

        //Checking for errors
        if (request.result == UnityWebRequest.Result.Success)
        {
            //Parsing the JSON response
            string responseText = request.downloadHandler.text;
            LoginResponse loginResponse = JsonUtility.FromJson<LoginResponse>(responseText);

            //Processing the response
            if (loginResponse.status)
            {
                //If Login success
                string token = loginResponse.token;
                string playerId = loginResponse.Player;
                Debug.Log("Login successful! Token: " + token + ", Player ID: " + playerId);
            }
            else
            {
                //If Login failed
                string errorMessage = loginResponse.msg;
                
                Debug.Log("Login failed. Error: " + errorMessage);
            }
        }
        else
        {
            //If Request failed
            Debug.LogError("Login request failed. Error: " + request.error);
        }
    }
}

// Data structure to hold login request data
[System.Serializable]
public class LoginData
{
    public string email;
    public string password;
    public int membertype;
    public string source;
}

// Data structure to parse login response data
[System.Serializable]
public class LoginResponse
{
    public bool status;
    public string token;
    public string Player;
    public string msg;
}
